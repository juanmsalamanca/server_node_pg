exports.getInsertQ = function (tableName, entity) {
    var query = "INSERT INTO " + tableName;
    var columns = " (";
    var values = " (";
    var iterator = new IteratorObj(entity);
    while (iterator.next()) {
        columns += iterator.getKey();
        values += "'" + iterator.getValue() + "'";
        if (iterator.getIndex() < iterator.getLength() - 1) {
            columns += ", ";
            values += ", ";
        }
    }
    query += columns + ") VALUES " + values + ");";
    return query;
}

exports.getUpdateQ = function (tableName, entity) {
    var query = "UPDATE " + tableName + " SET ";
    var columns = "";
    var iterator = new IteratorObj(entity);
    while (iterator.next()) {
        columns += iterator.getKey() + " = '" + iterator.getValue() + "' ";
        if (iterator.getIndex() < iterator.getLength() - 1) {
            columns += ", ";
        }
    }
    query += columns;
    query += " WHERE id = " + entity.id + " ;";
    return query;
}

exports.deleteRowQ = function (o) {
    console.log(o);
    return "DELETE FROM " + o.clazz + " WHERE id = " + o.id + ";"
}

exports.getEntityQ = function (o) {
    return "SELECT * FROM " + o.clazz + " WHERE id = " + o.id + "; ";
}


exports.getDataTable = function (tableName) {
    var query = "";
    switch (tableName) {
        case "Driver":
            query = "SELECT " + driverFileds + ", (id) AS Opciones FROM " + tableName + ";";
            break;
        case "Vehicle":
            query = "SELECT " + vehicleFields + ", (id) AS Opciones FROM " + tableName + ";";
            break;
        case "Route":
            query = "SELECT " + routeFields + ", (id) AS Opciones FROM " + tableName + ";";
            break;
        default:
            console.log('tableName undefined in getDataTable-queries')
    }
    return query;
}

function IteratorObj(obj) {
    var index = -1;
    var array = [];

    if (obj != undefined) {
        for (var attr in obj) {
            array.push(attr);
        }
    } else {
        console.log('OBJECT UNDEFINED');
    }

    this.next = function () {
        index++;
        return index < array.length ? true : false;
    }
    this.getValue = function () {
        return obj[array[index]];
    }
    this.getKey = function () {
        return array[index];
    }
    this.getIndex = function () {
        return index;
    }
    this.getLength = function () {
        return array.length;
    }

}


var driverFileds = "(id) AS Id, (creationUTCTime) AS Fecha, (name) AS Nombre, "
    + "(document) AS Documento, (phone) AS Telefono, (address) AS Direccion, (email) AS Email, "
    + "(driverLicense) AS Licencia,  (locked) AS Bloqueado  ";

var vehicleFields = "(id) AS Id, (plate) AS Placa,  " +
    "(latitude) AS Lat, (longitude) AS Lng, driverId ";

var routeFields = "(id) AS Id, (name) AS Nombre";