
var mysql = require('mysql');
var queries = require('./Queries');

function MySqlConnection(obj) {
    var connData = obj;
    var connection;

    var conn_pol = obj;
    conn_pol.connectionLimit = 1000;
    var pool_Connection = mysql.createPool(conn_pol)



    // this.save = function(entity, callback){
    //     var query = queries.getInsertQ(entity.clazz, entity.obj);
    //     this.executeQuery(query, function(o){
    //         console.log("SAVE");
    //         if(o.result === 'success'){
    //             console.log("save - success")
    //             var con = new MySqlConnection(connData);
    //             var query = "SELECT id FROM "+entity.clazz+" ORDER BY id DESC LIMIT 1 ; ";
    //             con.executeQuery(query, function(o2){
    //                 entity.obj.id = o2.data[0].id;
    //                 console.log(entity);
    //                 callback(o);
    //             })
    //         }else{
    //             callback(o);
    //         }
    //     });
    // };

    this.insert = function(entity, callBack){
        var o = {};
        var query = queries.getInsertQ(entity.clazz, entity.obj);
        pool_Connection.getConnection(function(err, connection){
            try {
                console.log("------------------------------------------");
                console.log("INSERT-Query: " + query);
                connection.query(query, function (err, rows, fields) {
                    if (err) {
                        console.log('error_Execute_query: ' + err);
                        o.result = err;
                        callBack(o);

                    } else {
                        entity.obj.id = rows.insertId;
                        o.result = 'success';
                        o.data = entity;
                        callBack(o);
                    }
                    console.log('finish destroy connection')
                    connection.release();
                });
            } catch (error) {
                console.log("Error: " + error);
                connection.release();
                o.result = 'error';
                callBack(o);
            }
        });
    };

    this.update = function(entity, callBack){
        var o = {};
        var query = queries.getUpdateQ(entity.clazz, entity.obj);
        pool_Connection.getConnection(function(err, connection){
            try {
                console.log("------------------------------------------");
                console.log("UPDATE-Query: " + query);
                connection.query(query, function (err, rows, fields) {
                    if (err) {
                        console.log('error_Execute_query: ' + err);
                        o.result = err;
                        callBack(o);

                    } else {
                        o.result = 'success';
                        o.data = entity;
                        callBack(o);
                    }
                    console.log('finish destroy connection')
                    connection.release();
                });
            } catch (error) {
                console.log("Error: " + error);
                connection.release();
                o.result = 'error';
                callBack(o);
            }
        });
    };

    this.drop = function(entity, callBack){
        var o = {};
        var query = queries.deleteRowQ(entity);
        pool_Connection.getConnection(function(err, connection){
            try {
                console.log("------------------------------------------");
                console.log("DROP query: " + query);
                connection.query(query, function (err, rows, fields) {
                    if (err) {
                        console.log('error_Execute_query: ' + err);
                        o.result = err;
                        callBack(o);

                    } else {
                        o.result = 'success';
                        o.data = rows;
                        callBack(o);
                    }
                    console.log('finish destroy connection')
                    connection.release();
                });
            } catch (error) {
                console.log("Error: " + error);
                connection.release();
                o.result = 'error';
                callBack(o);
            }
        });

    };

    this.getEntity_by_Id = function(entity, callBack){
        var o = {};
        var query =  queries.getEntityQ(entity);
        pool_Connection.getConnection(function(err, connection){
            try {
                console.log("------------------------------------------");
                console.log("getEntity_by_Id query: " + query);
                connection.query(query, function (err, rows, fields) {
                    if (err) {
                        console.log('error_Execute_query: ' + err);
                        o.result = err;
                        callBack(o);

                    } else {
                        o.result = 'success';
                        o.data = rows;
                        callBack(o);
                    }
                    console.log('finish destroy connection')
                    connection.release();
                });
            } catch (error) {
                console.log("Error: " + error);
                connection.release();
                o.result = 'error';
                callBack(o);
            }
        });
    }

    this.executeQuery = function (query, callback) {
        openConnection();
        var o = {};

        connection.connect(function (err) {
            if (err) {
                console.error('error connecting: ' + err.stack);
                return "error";
            } else {

                try {
                    console.log("Query: " + query);
                    connection.query(query, function (err, rows, fields) {
                        if (err) {
                            console.log('error: ' + err);
                            o.result = err;
                            callback(o);

                        } else {

                            console.log(rows);
                            o.result = 'success';
                            o.data = rows;
                            console.log(o);
                            callback(o);
                        }

                        console.log('finish destroy connection')
                        connection.destroy();
                    });
                } catch (error) {
                    console.log("Error: " + error);
                    connection.destroy();
                    o.result = 'error';
                    callback(o);
                }
            }
        });

    };

    this.getJsonArray = function(query, callBack){
        this.executeQuery(query, function(o){
            console.log(o)
            callBack(o.result, o.data);
        });


    }

    function openConnection() {
        connection = mysql.createConnection({
            host: connData.host,
            user: connData.user,
            password: connData.password,
            database: connData.database
        });
    }



    this.destroyConnection = function () {
        connection.destroy();
    };

    this.testConnection = function () {
        openConnection();
        connection.connect(function (err) {
            console.log('connected as id ' + connection.threadId);
            if (err) {
                console.error('error connecting: ' + err.stack);
                return;
            } else {
                console.log("Data Base test ok");
                connection.query('SHOW databases', function (err, rows, fields) {
                    if (err) throw err;
                    for (var p in rows) {
                        console.log('DataBase: ', rows[p]["Database"]);
                    }
                    connection.destroy();
                });
            }
        });
    }
}



exports.connection = {
    host: '54.213.81.66',
    user: 'user',
    password: '0000',
    database: 'test'
}
connectionToTest = {
    host: '54.213.81.66',
    user: 'user',
    password: '0000',
    database: 'test'
}
connectionProduction = {
    host: '54.213.81.66',
    user: 'user',
    password: '0000',
    database: 'production'
}
exports.MySqlConnection = MySqlConnection;
