var http = require('http');



function Fcm(serverKey){
    this.serverKey = serverKey;
    this.priority = 'normal';
    this.notification = {
        title: '',
        body: '',
        icon: '',
        sound: 'default'
    };
    this.data = false;




    this.send = function(data, fcmToken){
        var message = {};
        if(this.notification.title != ''){
            message.notification = this.notification;
        }
        message.data = data;
        message.priority = this.priority;
        message.to = fcmToken;

        console.log(message);

        var options = {
            host: 'fcm.googleapis.com',
            path: '/fcm/send',
            method: 'POST',
            headers: {
                'Host': 'fcm.googleapis.com',
                'Content-Type': 'application/json',
                'Authorization': this.serverKey,
            }
        };

        var post_req = http.request(options, function (res) {
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                console.log('Response: ' + chunk);
            });
        });

        // post the data

        post_req.write(JSON.stringify(message));
        post_req.end();



    }

}

exports.Fcm = Fcm;