var queries = require('../DataBase/Queries.js');
var Db = require('../DataBase/Db');


exports.save = function (entity, callback) {
    var newEntity = true;
    var connection = new Db.MySqlConnection(Db.connection);

    if (entity.obj.id != undefined && entity.obj.id != 0) {
        entity.obj['lastModificationUTCTime'] = new Date().getTime();
        newEntity = false;

    } else {
        entity.obj['creationUTCTime'] = new Date().getTime();
        entity.obj['lastModificationUTCTime'] = entity.obj['creationUTCTime'];
        entity.obj['locked'] = 0;

    }

    switch (entity.clazz) {

        case "Driver":
        case "Vehicle":
        case "Session":
            console.log("-------->" + JSON.stringify(entity.obj));
            connection = new Db.MySqlConnection(Db.connection);

            if(newEntity){
                connection.insert(entity, callback);
            }else{
                connection.update(entity, callback);
            }


            break;
        case "Route":
            console.log("-------->" + JSON.stringify(entity.obj));
            connection = new Db.MySqlConnection(Db.connection);

            if(newEntity){
                var path = entity.obj.route;
                console.log(path)
                delete entity.obj.route;
                var objResult = {};
                connection.insert(entity, function (o) {
                    console.log("insert route--------------------------------");
                    console.log(entity)
                    console.log(o)
                    objResult = o;
                    for(var i in path){
                        console.log("insert route_path--------------------------------");
                        path[i].routeId =  entity.obj.id;
                        var e = {clazz: "Route_path", obj: path[i]};
                        connection.insert(e,function(o){
                            console.log(o);
                        })

                    }
                    callback(o);

                });
            }else{
                console.log("update");
            }

            break;
        default:
            var o = {};
            o.result = 'error';
            o.data = "";
            callback(o);
            console.log("entity undefined" + entity);
    }
}

exports.delete = function (entity, callBack) {
    var connection = new Db.MySqlConnection(Db.connection);
    connection.drop(entity, callBack);
}

exports.get = function (entity, callBack) {
    var connection = new Db.MySqlConnection(Db.connection);
    switch(entity.clazz){
        case "Route":
            break;
        default:
            connection.getEntity_by_Id(entity, callBack);
    }

}


