var entityManager = require('../Entities/EntitiesManager.js');
var queries = require('../DataBase/Queries.js');
var Db = require('../DataBase/Db');



function manageTransaction(request, response) {
    console.log('manageTransaction');
    console.log(request.body);
    var service = request.body.service;
    var data = JSON.parse(request.body.data);
    var connection = new Db.MySqlConnection(Db.connection);

    switch (service) {
        case "getPlates":
            var query = "SELECT id, plate FROM Vehicle WHERE locked = false;";
            console.log('GetPlates');
            connection.getJsonArray(query, function(r, d){
                serviceResponse(response, 'application/json', r, d);
            });

            break;

        case "getDataTable":

            connection.executeQuery(queries.getDataTable(data.clazz), function (result) {
                serviceResponse(response, 'application/json', result, data);
            });
            console.log(queries.getDataTable(data.clazz));
            break;

        case "save-entity":
            if (data) {
                entityManager.save(data, function (r) {
                    serviceResponse(response, 'application/json', r, data);
                });

            }
            break;

        case "delete-entity":
            if (data) {
                entityManager.delete(data, function (r) {
                    serviceResponse(response, 'application/json', r, data);
                });

            }
            break;
        
        case "get-entity":
            if(data){
                entityManager.get(data, function(r){
                    serviceResponse(response, 'application/json', r, data);
                })
            }
            break;

        case "save-location":
            if(data){

            }
            break;
        
        case "getPagesPermitted":
            console.log('paginas permitidas');
            var pages = [];

            // var p1 = {name: 'Conductores', url: 'Driver.html'};
            // var p2 = {name: 'Vehiculos', url: 'Vehicle.html'};
            pages.push({name: 'Conductores', url: 'Driver.html'});
            pages.push({name: 'Vehiculos', url: 'Vehicle.html'});
            pages.push({name: 'Rutas', url: 'Route.html'});
            var d = {pages: pages};

            serviceResponse(response, 'application/json', 'success', d);
            break;
        case "":
            break;


        default:
            console.log('Service not define: ' + service);

    }

}



function serviceResponse(response, contenType, result, data) {
    var obj = {
        result: result,
        data: data
    };
    response.setHeader('Content-Type', contenType);
    response.send(obj);
    response.end();
}

exports.manageTransaction = manageTransaction;
exports.serviceResponse = serviceResponse;
