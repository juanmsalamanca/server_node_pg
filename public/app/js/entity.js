
function cancelSave(clazz){
    clearForm(clazz);
    togglePanel(clazz+'-panel');
}
// CRUD

function save(clazz){
    var obj;
    obj = new Entity(clazz);
    if(obj.init()){
        obj.save(this);
        inspection(obj);
        console.log(JSON.stringify(obj));
    }else{
        debugger;
    }
}

function deletee(clazz, id){
    var o = {
        clazz: clazz,
        id: id
    };
    show('spin-load');  
    sendPost(urlTransaction, 'delete-entity', o, function(t, r){
         if(r.result.result === 'success'){
                initTable(clazz);
                toastr.success("ok");    
        }else{
            toastr.error("error");    
        }
        console.log(r);
    });
}

function edit(clazz, id){
    var obj;
    obj = new Entity(clazz);
    obj.edit(id);
}

///////////////CRUD


function Entity(clazz) {
    this.clazz = clazz;


    this.init = function () {
        return formToObj(this);
    };

    this.save = function(){
        var d = this;
        delete d.clazz;
        var o = {
            clazz: clazz,
            obj: d
        };
        show('spin-load');  
        sendPost(urlTransaction, 'save-entity', o, function(t, r){
            console.log(r)
            if(r.result.result === 'success'){
                clearForm(clazz);
                initTable(clazz);
                toastr.success("ok");    
            }else{
                toastr.error("error");
                toastr.error(r.result.result.code);
            }
            console.log(r);
        });
    };

    this.edit = function(id){
        var o = {
            clazz: clazz,
            id: id
        }
        show('spin-load');  
        sendPost(urlTransaction, 'get-entity', o, function(t, r){
            showPanel(clazz + '-panel')
            objToForm(r.result.data[0], clazz);
        })
    };

}

function formToObj(obj) {
    var formElements = document.getElementById(obj.clazz + "-form").elements;
    var flag = validationForm(obj.clazz);
    if (flag) {

        switch(obj.clazz){
            case "Route":
                obj['route'] = routePath;
                break;

        }

        for (var i = 0; i < formElements.length; i++) {
            if (formElements[i].value != "null") {
                switch (formElements[i].type) {

                    case "checkbox":
                        if (formElements[i].checked) {
                            obj[formElements[i].name] = formElements[i].value;
                        }
                        break;
                    default:
                        if(formElements[i].value != undefined && formElements[i].value != ""){
                            obj[formElements[i].name] = formElements[i].value;
                        }
                }
            }
        }
    }
    return flag;
}

function validationForm(clazz) {
    var elements = document.getElementById(clazz + "-form").querySelectorAll(".required");
    var flag = true;
    for (var j = 0; j < elements.length; j++) {
        if (elements[j].value === "" || elements[j].value === "none") {
            elements[j].style.backgroundColor = ERROR_COLOR;
            flag = false;
        }
    }
    return flag;
}

function objToForm(obj, clazz){
    var form  = document.getElementById(clazz+ "-form").elements;
    for(var i = 0; i< form.length; i++){
        for(var key in obj){
            if(key === form[i].name){
                switch(form[i].type){
                    case "checkbox":
                        form[i].checked = obj[key];
                        break;
                    default:
                        form[i].value = obj[key];
                }
            }
        }
    }


}


function clearForm(clazz){
    var form  = document.getElementById(clazz+ "-form").elements;
    for(var i in form){
        form[i].value = "";
    }
}


var ERROR_COLOR = "rgba(200,0,0,0.6)";
function clearBGC(t){ $(t).css({"backgroundColor":""});
}