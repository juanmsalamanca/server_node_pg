var BOGOTA_POSITION = {lat: 4.5981, lng:-74.0758};
var map;
var poly;
var routePath = [];


function initMap_route(id){
    map = new google.maps.Map(document.getElementById(id), {
        center: BOGOTA_POSITION,
        zoom: 12
    });

    poly = new google.maps.Polyline({
        strokeColor: '#000000',
        strokeOpacity: 1.0,
        strokeWeight: 3
    });
    poly.setMap(map);

    map.addListener('click', function(event){
        var path = poly.getPath();
        path.push(event.latLng);
        routePath = [];
        for(var i in path.b){
            routePath.push({position: i, latitude: path.b[i].lat(), longitude: path.b[i].lng()});
            console.log(routePath)
        }


        generateRowsTable(routePath, 'table_body_path');
    });

    map.addListener('rightclick', function(event){
        var path = poly.getPath();
        path.pop();
        routePath = [];
        for(var i in path.b){
            routePath.push({position: i, latitude: path.b[i].lat(), longitude: path.b[i].lng()});
            console.log(routePath)
        }
        generateRowsTable(routePath, 'table_body_path');
    });


}

function generateRowsTable(path, id){
    var rows = "";

    for(var i in path){
        rows += "<tr>" +
            "<td>" + path[i].position +"</td>" +
            "<td>" + path[i].latitude +"</td>" +
            "<td>" + path[i].longitude +"</td>" +
            "</tr>";
    }
    document.getElementById(id).innerHTML = rows;


}
