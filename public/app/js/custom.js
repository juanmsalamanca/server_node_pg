
function show(id){
	if(document.getElementById(id) != undefined){
		$("#"+id+"").fadeIn("slow");
	}	
}
function hide(id){	
	if(document.getElementById(id) != undefined){
		$("#"+id+"").fadeOut("slow");
	}
}
function toggle(id){
	if(document.getElementById(id) != undefined){
		$("#"+id).toggle('slow');
	}
	
}



function togglePanel(idDiv){
	if(document.getElementById(idDiv).querySelector(".panel-body").style.display === "none"){		
		$("#"+idDiv+ " .fa ").first().removeClass("fa-chevron-down");
		$("#"+idDiv+ " .fa ").first().addClass("fa-chevron-up");
	}else{		
		$("#"+idDiv+ " .fa ").first().removeClass("fa-chevron-up");
		$("#"+idDiv+ " .fa ").first().addClass("fa-chevron-down");
	}
	$("#"+idDiv+ " .panel-body ").first().slideToggle("fast");
}
function showPanel(idDiv){
	if(document.getElementById(idDiv).querySelector(".panel-body").style.display === "none"){
		$("#"+idDiv+ " .fa ").first().removeClass("fa-chevron-down");
		$("#"+idDiv+ " .fa ").first().addClass("fa-chevron-up");
		$("#"+idDiv+ " .panel-body ").first().slideToggle("fast");
	}
}


function initTable(clazz){
	var tHead = "<thead> <tr>";
	var tBody = "<tbody>";


	$('#'+clazz+'-table').DataTable().clear().destroy();	
	show('spin-load');	
	sendPost(urlTransaction, 'getDataTable', {clazz: clazz}, function(o, r){
		console.log(r);
		var arrayDatas = r.result.data;


		if(arrayDatas.length > 0){

			for(var key in arrayDatas[0]){
				tHead += "<th> " + key + " </th>";
			}
			tHead += "</tr> </thead>";

			for(var i in arrayDatas){
				tBody += "<tr>";
				for(var key in arrayDatas[i]){

					switch(key){
						case "Fecha":
							var date = new Date(arrayDatas[i][key]);
							tBody += "<td> " + date.toLocaleString() + "</td> ";
							break;
						case "Opciones":
							tBody += "<td> "
								+ "<i title = 'Editar' onclick=\"edit('"+clazz+"', '"+arrayDatas[i][key]+"')\" style = 'cursor:pointer;' class=' fa fa-pencil ' ></i>&nbsp;"
								+ "<i title = 'Eliminar' onclick=\"toggle('verify-delete-"+arrayDatas[i][key]+"');\" style = 'cursor:pointer;' class=' fa fa-minus-circle ' ></i>&nbsp;"
								+ "<div id = \"verify-delete-"+arrayDatas[i][key]+"\"  style = 'display: none;' >"
								+ " Desea Continuar "
								+ "<button onclick=\"hide('verify-delete-"+arrayDatas[i][key]+"')\" type=\"button\" class=\"btn btn-xs btn-success\" >Cancelar</button>  "
								+ "<button onclick=\"deletee('"+clazz+"', '"+arrayDatas[i][key]+"')\"  type=\"button\" class=\"btn btn-xs btn-danger\" >Eliminar</button>  </div>"
								+ "</td> ";

							break;
						default:
							tBody += "<td> " + arrayDatas[i][key] + "</td> ";
					}


				}
				tBody += "</tr>";
			}
			tBody += "</tbody>";

			if(document.getElementById(clazz+"-table") != undefined){
				document.getElementById(clazz+"-table").innerHTML = tHead + tBody;

				$('#'+clazz+'-table').DataTable({
					searching: true,
					dom: "<'toolbar'>" +
					"<'row' <'col-sm-12 text-center'l>>" +
					"<'row' <'col-sm-12 text-center'B>>" +
					"<'row' <'col-sm-6 text-left'i>  <'col-sm-6 text-rigth'f> >tp",
					"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
					buttons: [
						{extend: 'copy',className: 'btn-sm'},
						{extend: 'csv',title: clazz, className: 'btn-sm'},
						{extend: 'excelHtml5', title: clazz, className: 'btn-sm'},
						{extend: 'print', title: clazz, className: 'btn-sm'}]
				});
			}

		}else{
			toastr.warning("No hay datos");

		}





	});	
}